# Ouihelp Technical Test

## Specification

on est proche de Noël, nous sommes un groupe d'amis, dont certains sont en couple, et nous voulons organiser un "secret santa" (un échange de cadeaux entre les membres du groupe). Pour ce "secret santa" comme nous sommes un groupe exigeant notre répartition des cadeaux (le "qui donne à qui") doit répondre à 3 règles :

- Chaque membre du groupe doit faire un et un seul cadeau, chaque membre du groupe doit recevoir un et un seul cadeau.
- Il ne peut pas y avoir de réciprocité dans la répartition : si A offre à B, alors B ne peut pas offrir à A.
- Il ne peut pas y avoir de cadeaux entre membres d'un couple : si A et B sont en couples, alors A n'offre pas à B et B n'offre pas à A.

Un jeux de données de test pour les membres et pour les couples :

    PEOPLE = ["Florent", "Jessica", "Coline", "Emilien", "Ambroise", "Bastien"]
    COUPLES = [("Florent", "Jessica"), ("Coline", "Emilien")]

Le but de l'exercice est donc d'écrire un "logiciel", dans le langage de ton choix, qui calcule et retourne une répartition possible des cadeaux (et tu peux te servir du jeu de données du dessus pour dev/tester ça).

## Set up

You can use `yarn` to install and run commands for this repository.

``` bash
# Install
yarn

# Test
yarn test
```

## Usage

You can give a try to this algorithm by running

``` bash
yarn dev
```

You can update [index.ts](./src/index.ts) `PARTICIPANTS` and `COUPLES` constants to try with different setup.
