
export type InitParticipant = { name: string, index: number, partner?: string };

export class Participant {
    private constructor(readonly name: string, readonly index: number, readonly partner?: string) { }

    static init({ name, index, partner }: InitParticipant) {
        return new Participant(name, index, partner)
    }

    canBeHisSanta(target: Participant, santas: SantaWithTarget[]) {
        const mySanta = santas.find(santa => santa.offerTo.name === this.name);
        return isTargetable(this, target, mySanta);
    }
}

export class SantaStrategy {

    static findTargets(santaCandidate: Participant, participants: Participant[], mySanta?: SantaWithTarget) {
        return participants.filter((participant) => isTargetable(santaCandidate, participant, mySanta));
    }

    static findBestTarget(santaCandidate: Participant, participantsWithoutGift: Participant[], santas: SantaWithTarget[]) {
        const mySanta = santas.find((otherSanta) => otherSanta.offerTo.name === santaCandidate.name);

        const targets = SantaStrategy.findTargets(santaCandidate, participantsWithoutGift, mySanta);

        const targetsOrderedBySantaCount = [...targets].sort((targetA, targetB) => {
            return comparePossibleSantaCount(targetA, targetB, participantsWithoutGift, santas)
        })

        const target = targetsOrderedBySantaCount.find(target => target.index > santaCandidate.index)
        return target ?? targetsOrderedBySantaCount.at(0)
    }

}

export class Santa {
    private me: Participant;

    private constructor(me: Participant) {
        this.me = me;
    }

    static fromParticipant(participant: Participant): Santa {
        return new Santa(participant)
    }

    assignTarget(participantsWithoutGift: Participant[], santas: SantaWithTarget[]): SantaWithTarget {
        return SantaWithTarget.fromParticipants(this, participantsWithoutGift, santas)
    }

    get identity(): Participant {
        return this.me;
    }
}

export class SantaWithTarget {
    private me: Participant;
    private target: Participant;

    private constructor(me: Participant, target: Participant) {
        this.me = me;
        this.target = target
    }

    static fromParticipants(me: Santa, participantsWithoutGift: Participant[], santas: SantaWithTarget[]) {
        const target = SantaStrategy.findBestTarget(me.identity, participantsWithoutGift, santas)

        if (!target) throw new Error(`Can't find target for ${JSON.stringify(me.identity)} in ${JSON.stringify(participantsWithoutGift)}`)

        return new SantaWithTarget(me.identity, target)
    }


    get offerTo(): Participant {
        return this.target;
    }

    get identity(): Participant {
        return this.me;
    }

    toString(): string {
        return `${this.identity.name} offer to ${this.target.name}`;
    }
}

function isTargetable(candidate: Participant, target: Participant, candidateSanta?: SantaWithTarget) {
    const isNotMe = target.name !== candidate.name;
    const isNotMyPartner = target.name !== candidate.partner;
    const isNotMySanta = candidateSanta?.identity.name !== target.name;

    return isNotMe && isNotMyPartner && isNotMySanta;

}

function comparePossibleSantaCount(targetA: Participant, targetB: Participant, participantsWithoutGift: Participant[], santas: SantaWithTarget[]) {
    const targetASantaCount = participantsWithoutGift.filter((participant) => participant.canBeHisSanta(targetA, santas)).length;
    const targetBSantaCount = participantsWithoutGift.filter((participant) => participant.canBeHisSanta(targetB, santas)).length;
    return targetASantaCount - targetBSantaCount;
}
