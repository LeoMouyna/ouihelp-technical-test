import { shuffle } from './list';
import { Santa, SantaWithTarget, Participant, SantaStrategy } from './santa';

const MINIMUM_PARTICIPANTS = 3;
const MINIMUM_PARTICIPANTS_WITH_COUPLES = 4;
const SINGLE_PARTICIPANT_MINIMUM_INDEX = 1_000_000;

type SecretSantaState = { potentialTargets: Participant[], santas: SantaWithTarget[], participantsWithoutGift: Participant[] }

export function secretSanta(participants: string[], couples: [string, string][]): SantaWithTarget[] {
    checkMinimumParticipation(participants, couples);

    const partners = new Map([...couples.flatMap(extractPartnersFromCouples)])

    const participantIdentities = shuffle(participants).map(generateParticipants(partners))

    const participantsOrderedByTargetsCount = [...participantIdentities]
        .sort(compareTargetsCount(participantIdentities))

    return solveSecretSanta({
        participantsWithoutGift: participantsOrderedByTargetsCount,
        santas: [],
        potentialTargets: participantIdentities
    });

}

function checkMinimumParticipation(participants: string[], couples: [string, string][],) {
    const minmumParticipants = couples.length > 0 ? MINIMUM_PARTICIPANTS_WITH_COUPLES : MINIMUM_PARTICIPANTS;
    if (participants.length < minmumParticipants) throw new Error("Not enough participants");
}

function extractPartnersFromCouples([partnerA, partnerB]: [string, string]): [string, string][] {
    return [[partnerA, partnerB], [partnerB, partnerA]];
}


function compareTargetsCount(participantIdentities: Participant[]): (a: Participant, b: Participant) => number {
    return (participantA, participantB) => {
        const participantATargetsCount = SantaStrategy.findTargets(participantA, participantIdentities).length;
        const participantBTargetsCount = SantaStrategy.findTargets(participantB, participantIdentities).length;
        return participantATargetsCount - participantBTargetsCount;
    };
}

function generateParticipants(partners: Map<string, string>): (name: string, index: number) => Participant {
    return (name, originalIndex) => {
        const partner = partners.get(name)
        const index = lowerSingleParticipantPriority(originalIndex, partner)
        return Participant.init({ name, index, partner });
    };
}

function lowerSingleParticipantPriority(originalIndex: number, partner?: string) {
    return partner ? originalIndex : originalIndex + SINGLE_PARTICIPANT_MINIMUM_INDEX;
}

function solveSecretSanta(state: SecretSantaState): SantaWithTarget[] {
    if (state.participantsWithoutGift.length === 0) return state.santas;

    const [participant, ...remainingParticipants] = state.participantsWithoutGift;

    const santa = Santa.fromParticipant(participant).assignTarget(state.potentialTargets, state.santas);

    const remainingTargets = state.potentialTargets.filter((participant) => participant.name !== santa.offerTo.name);
    const orderedRemainingParticipants = [...remainingParticipants]
        .sort(compareTargetsCount(state.participantsWithoutGift))
    const assignedSantas = [...state.santas, santa]

    const nextStepState = { participantsWithoutGift: orderedRemainingParticipants, santas: assignedSantas, potentialTargets: remainingTargets }
    return solveSecretSanta(nextStepState);
}

