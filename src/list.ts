type ShuffleState<T> = { shuffled: T[], remainings: T[] }

export function shuffle<T>(list: T[]): T[] {

    return shuffleItems(list, [])
}

function shuffleItems<T>(original: T[], shuffled: T[]): T[] {
    if (original.length === 0) return shuffled

    const index = Math.floor(Math.random() * (original.length));
    const item = original[index];
    const remainingsItems = removeItemAtIndex(original, index)

    return shuffleItems(remainingsItems, [...shuffled, item])
}


function removeItemAtIndex<T = any>(list: T[], index: number): T[] {
    if (index < 0) return removeItemAtIndex(list, list.length + index);
    return [...list.slice(0, index), ...list.slice(index + 1)];
}
