import { describe, it, expect } from 'vitest';
import { secretSanta } from './secret-santa';

const SANDRINE = 'Sandrine'
const SACHA = 'Sacha'
const VALERY = 'Valery'
const LUDOVIC = 'Ludovic'
const JEROME = 'Jerome'
const OLIVIA = 'Olivia'
const YOLANDE = 'Yolande'

describe("Secret Santa", () => {
    describe('When not enought participants', () => {
        it('should indicate their is not enough participants', () => {
            expect(() => secretSanta([LUDOVIC, YOLANDE], [])).toThrowError("Not enough participants")
        })
    })
    describe('When not enought participants with a couple', () => {
        it('should indicate their is not enough participants', () => {
            expect(() => secretSanta([LUDOVIC, YOLANDE, SANDRINE], [[YOLANDE, SANDRINE]])).toThrowError("Not enough participants")
        })
    })
    describe.each`
    participants                                          | couples
    ${[SANDRINE, SACHA, VALERY]}                          | ${[]}
    ${[SANDRINE, SACHA, VALERY, LUDOVIC]}                 | ${[]}
    ${[SANDRINE, SACHA, JEROME, LUDOVIC, VALERY]}         | ${[]}
    ${[LUDOVIC, YOLANDE, SACHA, SANDRINE, OLIVIA, VALERY]}| ${[]}
    ${[SANDRINE, SACHA, VALERY, LUDOVIC]}                 | ${[[SANDRINE, SACHA]]}
    ${[SANDRINE, SACHA, VALERY, LUDOVIC]}                 | ${[[SANDRINE, SACHA], [VALERY, LUDOVIC]]}
    ${[SANDRINE, SACHA, YOLANDE, VALERY, LUDOVIC]}        | ${[[SANDRINE, SACHA], [VALERY, LUDOVIC]]}
    ${[VALERY, LUDOVIC, SANDRINE, SACHA]}                 | ${[[SANDRINE, SACHA]]}
    ${[LUDOVIC, VALERY, SACHA, YOLANDE, SANDRINE]}        | ${[[SANDRINE, SACHA], [VALERY, LUDOVIC]]}
    ${[SANDRINE, LUDOVIC, VALERY, YOLANDE, SACHA]}        | ${[[SANDRINE, SACHA], [VALERY, LUDOVIC]]}
    ${[LUDOVIC, SANDRINE, YOLANDE, VALERY, SACHA]}        | ${[[SANDRINE, SACHA], [VALERY, LUDOVIC]]}
    `(`When 
        $participants are participants
        $couples are couples
    `, ({ participants, couples }) => {
        const santas = secretSanta(participants, couples)
        it('should define all participants as santas', () => {
            expect(santas.map(santa => santa.identity.name)).toEqual(expect.arrayContaining(participants))
        });
        it('should define all participants as receiver', () => {
            expect(santas.map(santa => santa.offerTo.name)).toEqual(expect.arrayContaining(participants))
        })
        it('should avoid reciprocity', () => {
            const santaWithReceiver = santas.map((santa): [string, string] => [santa.identity.name, santa.offerTo.name])
            expect(santaWithReceiver.every(([santa, receiver]) => {
                const receiverAsSanta = santaWithReceiver.find(([otherSanta,]) => otherSanta === receiver)
                if (!receiverAsSanta) return true
                return santa !== receiverAsSanta[1]
            })).toBe(true)
        });
        it('should avoid partners', () => {
            const santaWithReceiver = santas.map((santa): [string, string] => [santa.identity.name, santa.offerTo.name])
            expect(santaWithReceiver.every(([santa, receiver]) => {
                const partnerGiftIndex = couples.findIndex(([partner1, partner2]: [string, string]) => partner1 === santa && partner2 === receiver || partner2 === santa && partner1 === receiver)
                return partnerGiftIndex === -1
            })).toBe(true)
        })
    })
})


