import { secretSanta } from "./secret-santa"

const SANDRINE = 'Sandrine'
const SACHA = 'Sacha'
const VALERY = 'Valery'
const LUDOVIC = 'Ludovic'
const JEROME = 'Jerome'
const OLIVIA = 'Olivia'
const YOLANDE = 'Yolande'

const PARTICIPANTS = [SANDRINE, SACHA, VALERY, LUDOVIC, JEROME, OLIVIA, YOLANDE]
const COUPLES: [string, string][] = [[SANDRINE, JEROME], [OLIVIA, VALERY]]

function run() {
    console.log("Starting santa assignment for", PARTICIPANTS.join(', '))
    try {
        const santas = secretSanta(PARTICIPANTS, COUPLES);
        console.warn("This list is confidential")
        console.log(santas.map(santa => santa.toString()).join('\n'))
    } catch (e) {
        if (e instanceof Error) {
            console.error(e.message)
            return
        }
        console.error(e)
    }

}

run()